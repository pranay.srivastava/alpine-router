
CONTAINER_NAME ?= pvplatform-alpine-router
CONTAINER_VERSION ?= ARM32V6
EXPORT_PATH := $(shell mktemp -t $(CONTAINER_NAME).tgz.XXXXXX)
EXPORT_SQUASH_PATH := $(shell mktemp -t $(CONTAINER_NAME).squashfs.XXXXXX)
EXPORT_RAW_DIR := $(shell mktemp -d -t $(CONTAINER_NAME).raw.XXXXXX)

ifeq ($(DEBUG),)
QUIET = @
endif

build:
	mkdir tmp; wget -O tmp/qemu-arm-static https://github.com/multiarch/qemu-user-static/releases/download/v2.12.0-1/qemu-arm-static; chmod a+x tmp/qemu-arm-static
	docker build -f Dockerfile.ARM32V6 --no-cache -t $(CONTAINER_NAME):$(CONTAINER_VERSION) .

$(EXPORT_PATH): build
	$(QUIET)docker rm -f $(CONTAINER_NAME)-build || true; did=`docker run --name $(CONTAINER_NAME)-build -t -d $(CONTAINER_NAME):$(CONTAINER_VERSION) sh`; docker export $$did -o $(EXPORT_PATH)
	$(QUIET)echo "Exported available at: $(EXPORT_PATH)"

export: $(EXPORT_PATH)

$(EXPORT_SQUASH_PATH): $(EXPORT_PATH)
	rm -rf $(EXPORT_RAW_DIR); mkdir -p $(EXPORT_RAW_DIR)
	tar -C $(EXPORT_RAW_DIR) -xf $(EXPORT_PATH)
	mksquashfs $(EXPORT_RAW_DIR)/ $(EXPORT_SQUASH_PATH) -comp xz

export-squash: $(EXPORT_SQUASH_PATH)


